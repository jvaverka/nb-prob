# Events

 #prob

The set of all possible outcomes of a random experiment is called the **Sample
Space** and is denoted $S$.

A **subset** of $S$ is called an _**Event**_. An _event_ is said to have
occurred if the outcome of the experiment in the corresponding _subset_.

**Events** $A_1, A_2, \ldots$ are **Mutually Exclusive** or **disjoint** if:

$$
A_i \cap A_j = \o
$$


whenever $i \ne j$.

I.e., Mutually exclusive events cannot occur simultaneously.

Events $A_1, A_2, \ldots$ are **Exhaustive** if 
